This is a repositories for my accurate PMDG 777X aircraft configurations that are not included by PMDG.

To use these configs: download or copy the file you want to FSX\PMDG\PMDG 777X\Aircraft, and from the airplane's FMC, select PMDG OPTIONS > LOAD AIRCRAFT.

Internal project number: 144.000

preston@safetyfactorzero.com